echo "Running unittests"
echo "----------------------------------------------------------------------"
./project/scripts/run-unittests.sh

if [ $? -ne 0 ]
then
        echo "Unittests failed, aborting"
        exit 1
fi
echo "Unittests succeeded"


echo 'Starting Container.........'
./deployLocal.sh > /dev/null
sleep 10

echo "Running deployment tests"
echo "----------------------------------------------------------------------"
python3 -m unittest discover -s project -v -p dtest_*.py

if [ $? -ne 0 ]
then
        echo "Deployment tests failed, aborting"
        exit 1
fi

echo "Deployment tests succeeded"

