#!/bin/bash

./stop.sh
docker-compose -f project/docker/docker-compose.yml --project-directory . down -v
docker-compose -f project/docker/docker-compose.yml --project-directory . up -d
