echo 'Starting Container.........'
./deployLocal.sh > /dev/null
sleep 15

echo 'Testing....'
python3 -m unittest discover -s project -v -p dtest_*.py

if [ $? -ne 0 ]
then
	exit 1
fi

