echo "Running pre-coomit hook"

echo "Running unittests"
echo "----------------------------------------------------------------------"
./project/scripts/run-unittests.sh

if [ $? -ne 0 ]
then
        echo "Unittests failed, aborting"
        exit 1
fi
echo "Unittests succeeded"

echo "Running deployment tests"
echo "----------------------------------------------------------------------"
./project/scripts/run-deploymenttests.sh

if [ $? -ne 0 ]
then
        echo "Deployment tests failed, aborting"
        exit 1
fi

echo "Deployment tests succeeded"


