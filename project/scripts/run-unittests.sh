coverage run --omit */test_*,*__init__.py,/usr/*,/home/lubuntu/.local/*,*myoswrap.py,*custom* -m unittest discover -s ./project -v

if [ $? -ne 0 ]
then
	exit 1
fi

mkdir -p ./reporting/coverage
rm ./reporting/coverage/report.txt

coverage report >> ./reporting/coverage/report.txt
coverage html -d ./reporting/coverage/html

