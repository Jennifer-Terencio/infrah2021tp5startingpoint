CREATE DATABASE usersDB;
use usersDB;

CREATE TABLE users (
	username VARCHAR(32) NOT NULL,
	isInitialUser BIT DEFAULT 0 NOT NULL,
	UNIQUE(username)
);

INSERT INTO users VALUES
('Bob', 1),
('Bill',0),
('Body',1),
('Joe',0),
('Jack',0);
