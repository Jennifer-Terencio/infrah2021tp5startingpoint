import unittest
import requests
import json
import os
commitVersion = os.environ["CI_COMMIT_SHA"]
IP = "tp5-jt.ddnsgeek.com"
PORT = "5555"
URL = "http://" + IP + ":" + PORT

class APIDeployTest(unittest.TestCase):

	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)
		message = response.text
		self.assertIn(commitVersion,message)

if __name__ == "__main__":
	unittest.main()

