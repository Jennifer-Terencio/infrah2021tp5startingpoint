docker stop tp5jt
docker container rm tp5jt
docker image rm tp5jt_image
docker volume rm tp5jt_vol
docker volume create --name tp5jt_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp5jt_image -f ./project/docker/DockerfileAPI .
docker run -d -p 5555:5555 \
 -e "INFRA_TP5_DB_TYPE=MYSQL" \
 -e "INFRA_TP5_DB_HOST=db-tp5-infra.ddnsgeek.com" \
 -e "INFRA_TP5_DB_PORT=7777" \
 -e "INFRA_TP5_DB_USER='produser'" \
 -e "INFRA_TP5_DB_PASSWORD=3ac4d0b0e24871436f45275890a458c6" \
 --mount source=tp5jt_vol,target=/mnt/app/ --name tp5jt tp5jt_image 
